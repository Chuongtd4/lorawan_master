
#define P_SCAN		0x01
#define P_CAD		0x02
#define P_RX		0x04
#define P_TX		0x08
#define P_PRE		0x10
#define P_MAIN		0x20
#define P_GUI		0x40
#define P_RADIO		0x80

// Definition of the configuration record that is read at startup and written
// when settings are changed.

struct espGwayConfig {
	uint16_t fcnt;				// =0 as init value	XXX Could be 32 bit in size
	uint16_t boots;				// Number of restarts made by the gateway after reset
	uint16_t resets;			// Number of statistics resets
	uint16_t views;				// Number of sendWebPage() calls
	uint16_t wifis;				// Number of WiFi Setups
	uint16_t reents;			// Number of re-entrant interrupt handler calls
	uint16_t ntpErr;			// Number of UTP requests that failed
	uint16_t ntps;

	uint32_t ntpErrTime;		// Record the time of the last NTP error
	uint8_t ch;					// index to freqs array, freqs[ifreq]=868100000 default
	uint8_t sf;					// range from SF7 to SF12
	uint8_t debug;				// range 0 to 4
	uint8_t pdebug;				// pattern debug, 

	uint16_t logFileRec;		// Logging File Record number
	uint16_t logFileNo;			// Logging File Number
	uint16_t logFileNum;		// Number of log files
	
	bool cad;					// is CAD enabled?
	bool hop;					// Is HOP enabled (Note: default be disabled)
	bool isNode;				// Is gateway node enabled
	bool refresh;				// Is WWW browser refresh enabled
	
	String ssid;				// SSID of the last connected WiFi Network
	String pass;				// Password of WiFi network
} gwayConfig;

// Define a log record to be written to the log file
// Keep logfiles SHORT in name! to save memory
#if STAT_LOG == 1

// We do keep admin of logfiles by number
// 
//uint32_t logFileNo = 1;		// Included in struct espGwayConfig LogFile number
//uint32_t logFileRec = 0;		// Number of records in a single logfile
//uint32_t logFileNum = 1;		// Number of log files
#define LOGFILEMAX 10
#define LOGFILEREC 100

#endif

