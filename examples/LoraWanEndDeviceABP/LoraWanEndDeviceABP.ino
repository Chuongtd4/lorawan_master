
#define RX_frequency 43317500
#define TX_frequenc	 43317500
#include "LoraWan.h"
#include "SoftwareSerial.h"

#include <SPI.h>


int sensorPin = A0;
uint8_t sensorValue = 0;

SoftwareSerial loraStream(2, 3);  //uno
//SoftwareSerial loraStream(D2, D1);  //ESP
LoraWan exLoraWan(&loraStream);

uint8_t exDevEUI[8] = { 0x00, 0x00, 0x83, 0x05, 0x80, 0x7F, 0x07, 0x80 };
uint8_t exAppEUI[8] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01 };
uint8_t exAppKey[16] = { 0x00, 0x00, 0x00, 0x08, 0x00, 0x06, 0x00, 0x00, 0x00, 0x00, 0x50, 0x00, 0x00, 0x30, 0x00, 0x04 };

uint32_t exDeviceAddress =0x26011DBF;		//26041757;
uint8_t exAppSKey[16]	= { 0x00, 0x00, 0x00, 0x08, 0x00, 0x06, 0x00, 0x00, 0x00, 0x00, 0x50, 0x00, 0x00, 0x30, 0x00, 0x04 };//mbs
uint8_t exNwkKey[16]	= { 0x3A, 0x84, 0x72, 0x80, 0xB9, 0xD0, 0xF8, 0x0B, 0x8E, 0x00, 0x1A, 0x74, 0x5A, 0xE8, 0xCC, 0x23 };
uint8_t AppEUI[8];
uint8_t DevEUI[8];
uint8_t AppKey[16];

uint32_t DeviceAddress=0;
uint8_t AppSKey[16];
uint8_t NwkKey[16];

//uint8_t Buff[16];
uint32_t nowSeconds = millis();

void exLoraWanHandleIncommingMsg(uint8_t* msg) {
	Serial.println("\nRX_data:");
	uint8_t sMsgCmdId = exLoraWan.getMsgCommandId(msg);
	uint8_t sMsgLen = exLoraWan.getMsgDataLength(msg);
	uint8_t* sMsgData = exLoraWan.getMsgData(msg);

	switch (sMsgCmdId) {
	case LoraWan::CMD_ENDD_RECV_CONFIRM_MSG: {
		Serial.print("rx_data:");
		Serial.print(sMsgData[0]);
	}
		break;

	case LoraWan::CMD_ENDD_RECV_UNCONFIRM_MSG: {

	}
		break;

	default:
		break;
	}
}

void setup() {
	Serial.begin(115200);
	loraStream.begin(9600);

	exLoraWan.incommingMsgHandlerRegister(exLoraWanHandleIncommingMsg);

	exLoraWan.setOperationMode(LoraWan_MODE_ENDDEVICE);
	exLoraWan.setDeviceClass(LoraWan_CLASS_C);



	int ret = exLoraWan.setDeviceEUI(exDevEUI, 8);
	Serial.print("\nsetDeviceEUI:");
	Serial.print(ret);

	ret = exLoraWan.getDeviceEUI(DevEUI,8);
	Serial.print("\ngetDeviceEUI:");
	Serial.print(ret);

	Serial.print("\nDevEUI:");
	for(int i=0;i<8;i++)Serial.print(DevEUI[i],HEX);

	ret = exLoraWan.setAppEUI(exAppEUI, 8);
	Serial.print("\nsetAppEUI:");
	Serial.print(ret);

	ret = exLoraWan.getAppEUI(AppEUI,8);
	Serial.print("\ngetAppEUI:");
	Serial.print(ret);
	delay(2000);

	Serial.print("\nAppEUI:");
	for(int i=0;i<8;i++)Serial.print(AppEUI[i],HEX);



	if(exLoraWan.setDeviceAddr(exDeviceAddress)>=0)
	{
		exLoraWan.getDeviceAddr(&DeviceAddress);
		Serial.print("\nDeviceAddress:");
		Serial.print(DeviceAddress,HEX);
	}
	else
		Serial.print("\nSet DeviceAddress false");


	if(exLoraWan.setNetworkSessionKey(exNwkKey, 16)>=0)
	{
		exLoraWan.getNetworkSessionKey(NwkKey,16);
		Serial.print("\nNetworkSessionKey:");
		for(int i=0;i<16;i++)Serial.print(NwkKey[i],HEX);
	}
	else
		Serial.print("\nSet NetworkSessionKey false");


	if(exLoraWan.setAppSessionKey(exAppSKey, 16)>=0)
	{


		exLoraWan.getAppSessionKey(AppSKey,16);
		Serial.print("\nAppSessionKey:");
		for(int i=0;i<16;i++)Serial.print(AppSKey[i],HEX);


	}
	else
		Serial.println("\nSet AppSessionKey false");

	exLoraWan.setActiveMode(0x01);delay(1000);

	exLoraWan.setChannelMask((uint16_t)0x0001); // 433175000 Mhz
	exLoraWan.setDataRate(LoraWan_DR_0); // SF12, BW125


	if(exLoraWan.setRx2Frequency((uint32_t)433175000)==1) // 43317A5000 Mhz
	{
	uint32_t frequency;
	exLoraWan.getRx2Frequency(&frequency);
	Serial.print("\nRx2Frequency:");
	Serial.println(frequency);
	}

	exLoraWan.setRx2DataRate(LoraWan_DR_0); // SF12, BW125
	exLoraWan.setTxPower(0); //20dBm
	exLoraWan.setAntGain(0);

	exLoraWan.enddsendUnconfirmedData(1,&sensorValue, 1);

}

void loop() {
	exLoraWan.update();
	if((millis()-nowSeconds)>3000){
	sensorValue =analogRead(sensorPin);
	Serial.print("\nUnconfrm:");
	Serial.println(exLoraWan.enddsendUnconfirmedData(1,&sensorValue, sizeof(uint8_t)));
	nowSeconds = millis();
	}
}
