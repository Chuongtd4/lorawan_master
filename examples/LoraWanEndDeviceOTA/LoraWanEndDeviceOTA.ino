#include "LoraWan.h"
#include "SoftwareSerial.h"

SoftwareSerial loraStream(2, 3);
LoraWan exLoraWan(&loraStream);

uint8_t exDevEUI[8] = { 0x00, 0x00, 0x00, 0x08, 0x30, 0x05, 0x08, 0x00 };
uint8_t exAppEUI[8] = { 0x70, 0xB3, 0xD5, 0x70, 0x50, 0x00, 0x00, 0x01 };
uint8_t exAppKey[16] ={ 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05 };

uint8_t AppEUI[8];
uint8_t DevEUI[8];
uint8_t AppKey[16];


uint8_t sensorValue = 0;
const int analogInPin = A0;

void setup() {

	Serial.begin(115200);
	loraStream.begin(9600);

	exLoraWan.setOperationMode(LoraWan_MODE_ENDDEVICE);
	exLoraWan.setDeviceClass(LoraWan_CLASS_C);
	exLoraWan.setChannelMask((uint16_t)0x0002); // 433175000 Mhz
	exLoraWan.setDataRate(LoraWan_DR_0); // SF12, BW125
	exLoraWan.setRx2Frequency((uint32_t)433375000); // 43317A5000 Mhz
	exLoraWan.setRx2DataRate(LoraWan_DR_0); // SF12, BW125
	exLoraWan.setTxPower(0); //20dBm
	exLoraWan.setAntGain(0);


	if(exLoraWan.setDeviceEUI( exDevEUI, 8)>=0)
	{
		exLoraWan.getDeviceEUI(DevEUI,8);
		Serial.print("\nDevEUI:");
		for(int i=0;i<8;i++)Serial.print(DevEUI[i],HEX);
	}
	else
		Serial.println("\nSet DevEUI false");

	if(exLoraWan.setAppEUI( exAppEUI, 8)>=0)
		{
			exLoraWan.getAppEUI(AppEUI,8);
			Serial.print("\nAppEUI:");
			for(int i=0;i<8;i++)Serial.print(AppEUI[i],HEX);
		}
	else
			Serial.println("\nSet AppEUI false");

	if(exLoraWan.setAppKey( exAppKey, 16)>=0)
		{
			exLoraWan.getAppKey(AppKey,16);
			Serial.print("\nAppKey:");
			for(int i=0;i<16;i++)Serial.print(AppKey[i],HEX);
		}
	else
			Serial.println("\nSet AppKey false");


	exLoraWan.setActiveMode(0x00);//OTA

	exLoraWan.joinLoRaNetwork(LoraWan_JOIN_TIMEROUT);

}

void loop() {

	sensorValue = analogRead(analogInPin);
	Serial.println(exLoraWan.enddsendUnconfirmedData(1,LoraWan_DR_0,&sensorValue, 1));
	delay(3000);
}
