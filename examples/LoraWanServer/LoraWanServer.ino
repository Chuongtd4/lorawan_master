// 1-channel LoRaWan Server for ESP8266
// Copyright  2018  version for ESP8266
// Version 5.3.2
// Date: 2018-08-07
// Author: chuongtd4 (chuongtd4@fpt.com.vn)

#include "LoraWanServer.h"
#include <LoraWan.h>
#include <SoftwareSerial.h>
//#include <SPI.h>

uint8_t sensorValue = 0;
uint32_t frequency;
uint8_t dataRate,power;
uint32_t ttimeout =60;

//SoftwareSerial loraStream(2, 3);//rx tx
SoftwareSerial loraStream(D2, D1, false, 256);

LoraWan exLoraWan(&loraStream);

void SetupLoraModule(void )
{
	Serial.println("**********************SetupLoraModule**********************");
	Serial.print("setOperationMode:");
	Serial.println(exLoraWan.setOperationMode(LoraWan_MODE_SERVER));
	delay(100);

	Serial.print("setSendingConfigParams:");
	Serial.println(exLoraWan.SERV_setSendingConfigParams((uint32_t)433175000,0x00,0x14));//( frequency==433375000HZ,dataRate==0,power==20dBm)
	delay(100);
	Serial.print("get_Tx:");
	Serial.println(exLoraWan.SERV_getSendingConfigParams(&frequency,&dataRate,&power));
	Serial.print("Tx_frequency:");
	Serial.print(frequency);
	Serial.print("\nTx_dataRate:");
	Serial.println(dataRate);
	Serial.print("Tx_power:");
	Serial.println(power);
	delay(100);

	Serial.print("\nSERV_setReceivingConfigParams:");
	Serial.println(exLoraWan.SERV_setReceivingConfigParams((uint32_t)433175000,0x000000));
	delay(100);
	frequency=0;
	dataRate=0;
	Serial.print("get_Rx:");
	Serial.println(exLoraWan.SERV_getReceivingConfigParams(&frequency, &dataRate));
	Serial.print("Rx_frequency:");
	Serial.print(frequency);
	Serial.print("\nRx_dataRate:");
	Serial.println(dataRate);
	Serial.println("***************************FINISH****************************");

	delay(3000);

	Serial.print("\nsetNetworkParameter"),
	Serial.println(exLoraWan.SERV_setNetworkParameter(exNetworkID,exAppKey));
	delay(1000);
	exLoraWan.SERV_getNetworkParameter(NetworkID,AppKey);
	Serial.print("NetworkID: ");
	for(int i=0;i<3;i++)
	{
	Serial.print(NetworkID[i]);
	}
	Serial.print("\nAppKey: ");
	for(int i=0;i<16;i++)
	{
	Serial.print(AppKey[i]);
	}
}
int join_OTA(){
	exLoraWan.requestJoinOTA(exDevEUI,exAppEUI,ttimeout);
	exLoraWan.getJoinOTAPrams(DevEUI,AppEUI,&Timer_join);
	Serial.print("\nrequestJoinOTA\n");
	Serial.print("DevEUI:");
	for(int i=0;i<8;i++){
	Serial.print(DevEUI[i]);
	}

	Serial.print("\nAppEUI:");
	for(int i=0;i<8;i++){
	Serial.print(AppEUI[i]);
	}

	Serial.print("\nTimer OUT:");
	Serial.print(Timer_join);
}
void exLoraWanHandleIncommingMsg(uint8_t* msg) {
	Serial.println("\nRX_data:");
	uint8_t sMsgCmdId = exLoraWan.getMsgCommandId(msg);
	uint8_t sMsgLen = exLoraWan.getMsgDataLength(msg);
	uint8_t* sMsgData = exLoraWan.getMsgData(msg);
}

void setup()
{
loraStream.begin(9600);
Serial.begin(115200);
exLoraWan.incommingMsgHandlerRegister(exLoraWanHandleIncommingMsg);
delay(10);
SetupLoraModule();
join_OTA();
}

void loop()
{
exLoraWan.update();
delay(10000);
}
